import { italic, cyan } from "colorette";

describe("Ticket",() => {
    beforeEach(()=> cy.visit("https://bit.ly/3rDYczE"));

it("Fills all the text input fields",() => {

    const FirstName="Rafael";
    const LastName="Bezerra Correia da Silva";
    const Email="teste1@redepos.com.br"

    cy.get("#first-name").type(FirstName);
    cy.get("#last-name").type(LastName);
    cy.get("#email").type(Email);
    cy.get("#requests").type("Aprendendo a usar o comando type no Cypress");
    cy.get("#signature").type(`${FirstName} ${LastName} estudando Cypress`);
})

it("Marcando a quantidade para 2 no  select e escolher ticket vip e escolha no checkbox",()=>
{
    cy.get("#ticket-quantity").select("2");
    cy.get("#friend").check();
    cy.get("#social-media").check();
    cy.get("#vip").check();
    cy.get("#friend").uncheck();
})

it("Has 'TICKETBOX' header's heading",()=>{
  //TICKETBOX
    cy.get("header h1").should("contain","TICKETBOX");
})
it("Verificação de alerta de email invalido",()=> {
  //Digitando e-mail invalido
  cy.get("#email").as("email").type("bodaope-gmail.com");
  cy.get("#email.invalid").should("exist");

  cy.get("@email").clear().type("rafael.bcsilva@gmail.com");

  cy.get("#email.invalid").should("not.exist");
});


  it("Preenchimento de todo formulário e depois reset do mesmo",()=>
  {
      //Preenchendo o formulário
    const FirstName="Rafael";
    const LastName="Bezerra Correia da Silva";
    const Email="teste1@redepos.com.br"
    const FullName=`${FirstName} ${LastName}`;

    cy.get("#first-name").type(FirstName);
    cy.get("#last-name").type(LastName);
    cy.get("#email").type(Email);
    cy.get("#requests").type("Aprendendo a usar o comando type no Cypress");
    cy.get("#signature").type(`${FirstName} ${LastName} estudando Cypress`);
    cy.get("#ticket-quantity").select("2");
    cy.get("#friend").check();
    cy.get("#social-media").check();
    cy.get("#vip").check();
    cy.get("#agree").check();
    cy.get(".agreement p").should("contain",`I, ${FullName}, wish to buy 2 VIP tickets`);
    cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");
    cy.get("button[type='reset']").click();
    cy.get("@submitButton").should("be.disabled");

  });

  it("Função Preencher Formulario",() => {
     const cliente ={
       firstName: "Rafael",
       lastName: "Bezerra Correia da Silva",
       email: "teste1@redepos.com.br"

     };
     cy.preencherFormulario(cliente);
     cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");
    cy.get("#agree").uncheck();
    cy.get("@submitButton").should("be.disabled");


  })

});